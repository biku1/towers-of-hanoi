#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <stack>
using namespace std;

void draw(int t1[], int t2[], int t3[])
{
		printf("%d   %d   %d\n", t1[0], t2[0], t3[0]);
		printf("%d   %d   %d\n", t1[1], t2[1], t3[1]);
		printf("%d   %d   %d\n", t1[2], t2[2], t3[2]);
		printf("\n");
}



int fromTXtoTY(int t1[], int t2[], int t3[], int disc, int pos, int tower)
{
	int discMay = 0; //the disc on the top of the tower

	if (tower == 1)
	{
		//trying to move the disc to from tower 1 to tower 3
		for (int i = 0; i < 3 && discMay == 0; i++)
		{
			if (t3[i] != 0) discMay = t3[i]; //getting the position of the first disc in the tower
		}
		if (discMay != 0)
		{
			if (discMay > disc) return 3; //if the disc of the tower is higher you can move the disc selected to that tower
		}
		else return 3; //if is no disc in the tower 3 you can move to tower 3 the disc

		//trying to move the disc to from tower 1 to tower 2
		discMay = 0;
		for (int i = 0; i < 3 && discMay == 0; i++)
		{
			if (t2[i] != 0) discMay = t2[i]; //getting the position of the first disc in the tower
		}
		if (discMay != 0)
		{
			if (discMay > disc) return 2; //if the disc of the tower is higher you can move the disc selected to that tower
		}
		else return 2; //if is no disc in the tower 3 you can move to tower 3 the disc

		return -1; //if you can't move that disc
	}


	else if (tower == 2)
	{
		discMay = 0;
		//trying to move the disc to from tower 2 to tower 3
		for (int i = 0; i < 3 && discMay == 0; i++)
		{
			if (t3[i] != 0) discMay = t3[i]; //getting the position of the first disc in the tower
		}
		if (discMay != 0)
		{
			if (discMay > disc) return 3; //if the disc of the tower is higher you can move the disc selected to that tower
		}
		else return 3; //if is no disc in the tower 3 you can move to tower 3 the disc

		//trying to move the disc to from tower 2 to tower 1
		discMay = 0;
		for (int i = 0; i < 3 && discMay == 0; i++)
		{
			if (t1[i] != 0) discMay = t1[i]; //getting the position of the first disc in the tower
		}
		if (discMay != 0)
		{
			if (discMay > disc) return 1; //if the disc of the tower is higher you can move the disc selected to that tower
		}
		else return 1; //if is no disc in the tower 3 you can move to tower 3 the disc

		return -1; //if you can't move that disc
	}



	else{
		//trying to move the disc to from tower 3 to tower 2
		discMay = 0;
		for (int i = 0; i < 3 && discMay == 0; i++)
		{
			if (t2[i] != 0) discMay = t2[i]; //getting the position of the first disc in the tower
		}
		if (discMay != 0)
		{
			if (discMay > disc) return 2; //if the disc of the tower is higher you can move the disc selected to that tower
		}
		else return 2; //if is no disc in the tower 3 you can move to tower 3 the disc

		//trying to move the disc to from tower 3 to tower 1
		discMay = 0;
		for (int i = 0; i < 3 && discMay == 0; i++)
		{
			if (t1[i] != 0) discMay = t1[i]; //getting the position of the first disc in the tower
		}
		if (discMay != 0)
		{
			if (discMay > disc) return 1; //if the disc of the tower is higher you can move the disc selected to that tower
		}
		else return 1; //if is no disc in the tower 3 you can move to tower 3 the disc

		return -1; //if you can't move that disc
	}
}



int firstDiscAvaibleToMove(int t1[], int t2[], int t3[], int lastDisc)
{
	
	int discToMove = 0;
	int position = 0;
	int tower = 0;
	int toTower = -1;

	//If the disc to move is on tower 1
	if (t1[0] != 0 && t1[0] != lastDisc){ discToMove = t1[0]; position = 0; tower = 1; }
	else if (t1[1] != 0 && t1[1] != lastDisc){ discToMove = t1[1]; position = 1; tower = 1; }
	else if (t1[2] != 0 && t1[2] != lastDisc){ discToMove = t1[2]; position = 2; tower = 1; }

	if (discToMove != 0){
		toTower = fromTXtoTY(t1, t2, t3, discToMove, position, tower);
		//if the tower to be moved the disc is the third
		if (toTower == 3){
			if (t3[2] == 0){ t3[2] = discToMove; t1[position] = 0; }
			else if (t3[1] == 0){ t3[1] = discToMove; t1[position] = 0; }
			else { t3[0] = discToMove; t1[position] = 0; }
			return discToMove;
		}
		//if the tower to be moved the disc is the second
		if (toTower == 2){
			if (t2[2] == 0){ t2[2] = discToMove; t1[position] = 0; }
			else if (t2[1] == 0){ t2[1] = discToMove; t1[position] = 0; }
			else { t2[0] = discToMove; t1[position] = 0; }
			return discToMove;
		}
	}
	

	//if the disc to move is on tower 2
	if (t2[0] != 0 && t2[0] != lastDisc){ discToMove = t2[0]; position = 0; tower = 2; }
	else if (t2[1] != 0 && t2[1] != lastDisc){ discToMove = t2[1]; position = 1; tower = 2; }
	else if (t2[2] != 0 && t2[2] != lastDisc){ discToMove = t2[2]; position = 2; tower = 2; }

	if (discToMove != 0){
		toTower = fromTXtoTY(t1, t2, t3, discToMove, position, tower);
		//if the tower to be moved the disc is the third
		if (toTower == 3){
			if (t3[2] == 0){ t3[2] = discToMove; t2[position] = 0; }
			else if (t3[1] == 0){ t3[1] = discToMove; t2[position] = 0; }
			else { t3[0] = discToMove; t2[position] = 0; }
			return discToMove;
		}
		//if the tower to be moved the disc is the first
		if (toTower == 1){
			if (t1[2] == 0){ t1[2] = discToMove; t2[position] = 0; }
			else if (t1[1] == 0){ t1[1] = discToMove; t2[position] = 0; }
			else { t1[0] = discToMove; t2[position] = 0; }
			return discToMove;
		}
	}

	//if the disc to move is on tower3
	if (t3[0] != 0 && t3[0] != lastDisc){ discToMove = t3[0]; position = 0; tower = 3; }
	else if (t3[1] != 0 && t3[1] != lastDisc){ discToMove = t3[1]; position = 1; tower = 3; }
	else if (t3[2] != 0 && t3[2] != lastDisc){ discToMove = t3[2]; position = 2; tower = 3; }

	toTower = fromTXtoTY(t1, t2, t3, discToMove, position, tower);
	//if the tower to be moved the disc is the second
	if (toTower == 2){
		if (t2[2] == 0){ t2[2] = discToMove; t3[position] = 0; }
		else if (t2[1] == 0){ t2[1] = discToMove; t3[position] = 0; }
		else { t2[0] = discToMove; t2[position] = 0; }
		return discToMove;
	}
	//if the tower to be moved the disc is the first
	if (toTower == 1){
		if (t1[2] == 0){ t1[2] = discToMove; t3[position] = 0; }
		else if (t1[1] == 0){ t1[1] = discToMove; t3[position] = 0; }
		else { t1[0] = discToMove; t3[position] = 0; }
		return discToMove;
	}

	return -1;
}

int iteration(int t1[], int t2[], int t3[], int lastDisc)
{
	int disc = firstDiscAvaibleToMove(t1, t2, t3, lastDisc);
	return disc;
}

int main(int argc, char * argv[]){
	//Tower initialize
	int tower1[3] = { 1, 2, 3 };
	int tower2[3] = { 0, 0, 0 };
	int tower3[3] = { 0, 0, 0 };
	//The last disc moved
	int ultDiscMov = 0;
	//Initial draw
	draw(tower1, tower2, tower3);
	//Number of movements done
	int numOfMov = 0;
	//When in the tower 3: 3 + 2 + 1 = 6 the work is done
	while (tower3[0] + tower3[1] + tower3[2] < 6)
	{
		ultDiscMov = iteration(tower1, tower2, tower3, ultDiscMov); //do an iteration and get the last disc moved
		if (ultDiscMov == -1){ printf("Break"); return 0; } //if something fails I go out of the programm with the message "break"
		numOfMov++;
		draw(tower1, tower2, tower3); //draw update
	}
	printf("Number of Movements = %i\n", numOfMov);
	system("PAUSE");
	return 0;
}